# Standard FIND_PACKAGE module for mongo-c, sets the following variables:
#   - MONGOC_FOUND
#   - MONGOC_INCLUDE_DIRS (only if MONGOC_FOUND)
#   - MONGOC_LIBRARIES (only if MONGOC_FOUND)

# Try to find the header
FIND_PATH(MONGOC_INCLUDE_DIR NAMES libmongoc-1.0/mongoc.h)

# Try to find the library
FIND_LIBRARY(MONGOC_LIBRARY NAMES mongoc-1.0)

# Handle the QUIETLY/REQUIRED arguments, set LIBUV_FOUND if all variables are
# found
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MONGOC
                                  REQUIRED_VARS
                                  MONGOC_LIBRARY
                                  MONGOC_INCLUDE_DIR)

# Hide internal variables
MARK_AS_ADVANCED(MONGOC_INCLUDE_DIR MONGOC_LIBRARY)

# Set standard variables
IF(MONGOC_FOUND)
    SET(MONGOC_INCLUDE_DIRS "${MONGOC_INCLUDE_DIR}")
    SET(MONGOC_LIBRARIES "${MONGOC_LIBRARY}")
ENDIF()
