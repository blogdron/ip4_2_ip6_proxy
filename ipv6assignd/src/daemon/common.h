/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef COMMON_H_
#define COMMON_H_

enum {
    LEASE     = 1,
    RELEASE   = 2,
    LEASE_OK  = 4,
    LEASE_ERR = 8
};

typedef struct {
    uv_write_t req;
    uv_buf_t buf;
} write_req_t;

typedef struct {
    int cmd;
    struct in6_addr ip;
} msg_proto_t;

void on_close( uv_handle_t * );
void on_write( uv_write_t *, int );
void alloc_buffer( uv_handle_t *, size_t, uv_buf_t * );

#endif
