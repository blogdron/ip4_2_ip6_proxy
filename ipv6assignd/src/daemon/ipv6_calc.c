/* vim: set tabstop=4 shiftwidth=4 expandtab : */

#include <ipv6_calc.h>

void decrement_ip_address(struct in6_addr *addr)
{
    int octet;

    for (octet = 15; octet >= 0; --octet)
    {
        if (addr->s6_addr[octet] > 0)
        {
            addr->s6_addr[octet]--;
            break;
        }
        else
        {
            addr->s6_addr[octet] = 0xff;
        }
    }
}

void increment_ip_address(struct in6_addr *addr)
{
    int octet;

    for (octet = 15; octet >= 0; --octet)
    {
        if (addr->s6_addr[octet] < 0xff)
        {
            addr->s6_addr[octet]++;
            break;
        }
        else
        {
            addr->s6_addr[octet] = 0;
        }
    }
}

void get_min_and_max_ip(struct in6_addr *start, struct in6_addr *end,
                        const struct in6_addr *address, int mask)
{
    int octet;
    int imask = 128 - mask; 

    for (octet = 15; octet >= 0; --octet)
    {
        int i = (imask > 8) ? 8 : imask;
        unsigned char x = (1 << i) - 1;

        start->s6_addr[octet] = address->s6_addr[octet] & ~x;
        end->s6_addr[octet]   = address->s6_addr[octet] | x;

        imask -= i;
    }
}
