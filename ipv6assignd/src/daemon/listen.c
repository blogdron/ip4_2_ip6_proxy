/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <uv.h>

#include <send.h>
#include <common.h>
#include <read_config.h>
#include <manage_mongo_db.h>

static void new_connection( uv_stream_t *, int );
static void on_read( uv_stream_t *, ssize_t, const uv_buf_t * );
static void handle_stop( uv_signal_t *, int );
static void handle_hup( uv_signal_t *, int );
static void init_signals( daemon_data_t * );
static int setup_tcp_socket( const daemon_data_t * );
static int setup_unix_socket( const daemon_data_t * );
static int daemon_listen_setup( const daemon_data_t * );

static uv_loop_t *loop;

static union {
    uv_tcp_t  tcp_sock;
    uv_pipe_t unix_sock;
} server_sock;

int wait_for_clients( daemon_data_t *data )
{
    loop = uv_default_loop();
    init_signals( data );

    if( daemon_listen_setup( data ) != 0 )
    {
        syslog( LOG_ERR, "Socket setup failed" );
        return 1;
    }

    uv_run( loop, UV_RUN_DEFAULT );

    return 0;
}

static void on_read( uv_stream_t *client, ssize_t nread, const uv_buf_t *buf )
{
    msg_proto_t *msg;
    char client_ipstr[INET_ADDRSTRLEN];
    struct sockaddr_in client_addr;
    int client_addrsz = sizeof( client_addr );
    const daemon_data_t *data = ( const daemon_data_t * ) client->data;

    if( nread < 0 )
    {
        if( nread != UV_EOF )
        {
            syslog( LOG_WARNING, "Read error %s", uv_strerror( nread ));
        }
        uv_close(( uv_handle_t *) client, on_close );
        if( buf->base )
        {
            free( buf->base );
        }
        return;
    }

    if( nread != sizeof( msg_proto_t ))
    {
        syslog( LOG_WARNING, "Ivalid data size" );
        uv_close(( uv_handle_t *) client, on_close );
        free( buf->base );
        return;
    }

    /* For IPv6 support change struct sockaddr_in to sockaddr_storage */
    if( data->cfg->link_family == AF_INET )
    {
        uv_tcp_getpeername(( uv_tcp_t * ) client, ( struct sockaddr * ) &client_addr, &client_addrsz );
        inet_ntop( AF_INET, &client_addr.sin_addr, client_ipstr, INET_ADDRSTRLEN );
        syslog( LOG_INFO, "Message recieved from %s:%d", client_ipstr, client_addr.sin_port );
    }

    msg = ( msg_proto_t * ) buf->base;
    if( msg->cmd & LEASE )
    {
         syslog( LOG_INFO, "Client request address" );
         lease_ip_address( client );
    }

    free( buf->base );
}

static void new_connection( uv_stream_t *srv, int status )
{
    uv_tcp_t *client;

    if( status < 0 )
    {
        syslog( LOG_WARNING, "New connection error %s", uv_strerror( status ));
        return;
    }

    client = ( uv_tcp_t * ) malloc( sizeof( uv_tcp_t ));
    uv_tcp_init( loop, client );
    if( uv_accept( srv, ( uv_stream_t *) client ) == 0 )
    {
        syslog( LOG_INFO, "Client connected" );
        client->data = srv->data;
        uv_read_start(( uv_stream_t *) client, alloc_buffer, on_read );
    }
    else
    {
        uv_close(( uv_handle_t *) client, NULL );
    }
}

static void handle_stop( uv_signal_t *handle, int signum )
{
    syslog( LOG_NOTICE, "Received signal: %d, terminating", signum );
    uv_stop( loop );
    if( unlink( DEFAULT_PID_PATH ) == -1 )
    {
        syslog( LOG_WARNING, "Cannot delete pid file %s : %s",
                DEFAULT_PID_PATH, strerror( errno ));
    }
}

static void handle_hup( uv_signal_t *handle, int signum )
{
    syslog( LOG_NOTICE, "Received signal: %d, reloading config file", signum );
    daemon_data_t *data = ( daemon_data_t * ) handle->data;
    if( read_config( data ) != 0 )
    {
        fprintf( stderr, "Config file reading error\n" );
        exit( EXIT_FAILURE );
    }
    reload_database( data );
}

static void init_signals( daemon_data_t *data )
{
    static uv_signal_t sigint, sigterm, sigquit, sighup;

    uv_signal_init( loop, &sigint );
    uv_signal_start( &sigint, handle_stop, SIGINT );

    uv_signal_init( loop, &sigterm );
    uv_signal_start( &sigterm, handle_stop, SIGTERM );

    uv_signal_init( loop, &sigquit );
    uv_signal_start( &sigquit, handle_stop, SIGQUIT );

    uv_signal_init( loop, &sighup );
    sighup.data = ( void * ) data;
    uv_signal_start( &sighup, handle_hup, SIGHUP );
}

static int setup_tcp_socket( const daemon_data_t *data )
{
    int ret;
    struct sockaddr_in addr;

    uv_tcp_init( loop, &server_sock.tcp_sock );
    uv_ip4_addr( data->cfg->host, data->cfg->port, &addr );
    if(( ret = uv_tcp_bind( &server_sock.tcp_sock, ( const struct sockaddr * ) &addr, 0 )))
    {
        syslog( LOG_ERR, "TCP bind error: %s", uv_strerror( ret ));
        return 1;
    }
    server_sock.tcp_sock.data = ( void * ) data;
    if(( ret = uv_listen(( uv_stream_t * ) &server_sock.tcp_sock, SOMAXCONN, new_connection )))
    {
        syslog( LOG_ERR, "TCP listen error: %s", uv_strerror( ret ));
        return 1;
    }

    return 0;
}

static int setup_unix_socket( const daemon_data_t *data )
{
    int ret;

    unlink( data->cfg->host );

    uv_pipe_init( loop, &server_sock.unix_sock, 0 );
    if(( ret = uv_pipe_bind( &server_sock.unix_sock, data->cfg->host )))
    {
        syslog( LOG_ERR, "UNIX domain bind error: %s", uv_strerror( ret ));
        return 1;
    }
    server_sock.unix_sock.data = ( void * ) data;
    if(( ret = uv_listen(( uv_stream_t * ) &server_sock.unix_sock, SOMAXCONN, new_connection )))
    {
        syslog( LOG_ERR, "UNIX domain listen error: %s", uv_strerror( ret ));
        return 1;
    }

    return 0;
}

static int daemon_listen_setup( const daemon_data_t *data )
{
    int ret = 0;

    switch( data->cfg->link_family )
    {
        case AF_INET:
            ret = setup_tcp_socket( data );
            break;
        case AF_UNIX:
            ret = setup_unix_socket( data );
            break;
        default:
            syslog( LOG_ERR, "Unsupported socket type" );
            ret = 1;
    }

    return ret;
}
