/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef READ_CONFIG_H_
#define READ_CONFIG_H_

#include <limits.h>
#include <arpa/inet.h>

#include <libmongoc-1.0/mongoc.h>

#define DEFAULT_CFG_PATH     "/etc/ipv6assignd.cfg"
#define DEFAULT_PID_PATH     "/tmp/ipv6assignd.pid"
#define DEFAULT_DAEMON_HOST  "127.0.0.1"
#define DEFAULT_DAEMON_PORT  12345
#define DEFAULT_MONGO_URI    "mongodb://localhost:27017"
#define DEFAULT_MONGO_DB     "ipv6assignd"
#define DEFAULT_PERCENT      50
#define DAEMON_HOST_LEN      256
#define MONGO_DB_LEN         128
#define MONGO_URI_LEN        2048

#define ERREXIT() { fprintf( stderr, "Usage: %s [options]\n" \
                                      "options:\n" \
                                      "\t -g\tgenerate database\n" \
                                      "\t -n\tdo not daemonize\n" \
                                      "\t -c\tpath to config file\n" \
                                      "\t -h\tprint help\n", \
                                      "ipv6assignd" );\
                    exit(1); }

#ifndef config_error_file
#define config_error_file(C) ""
#endif /* config_error_file */

enum {
    RUNDAEMON,
    GENERATEDB,
    HELPMSG  = 4,
    NODAEMON = 8
};

typedef struct {
    struct in6_addr ip_start;
    struct in6_addr ip_end;
} ipv6_range_t;

typedef struct {
    char host[DAEMON_HOST_LEN];
    long port;
    long link_family;
    char mongo_uri[MONGO_URI_LEN];
    char mongo_db[MONGO_DB_LEN];
    ipv6_range_t ip_range;
    long iprange_count;
    long percent;
} daemon_config_t;

typedef struct {
    daemon_config_t *cfg;
    char cfg_path[PATH_MAX];
    mongoc_client_pool_t *pool;
} daemon_data_t;

int read_config( daemon_data_t * );

#endif /* READ_CONFIG_H_ */
