/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <uv.h>

#include <common.h>
#include <read_config.h>
#include <manage_mongo_db.h>

void lease_ip_address( uv_stream_t *dest )
{
    char strip[INET6_ADDRSTRLEN];
    msg_proto_t resp;
    write_req_t *req;
    const daemon_data_t *data = ( const daemon_data_t * ) dest->data;

    if( get_available_address( data, &resp.ip ) == 0 )
    {
        resp.cmd = LEASE_OK;
        inet_ntop( AF_INET6, &resp.ip, strip, INET6_ADDRSTRLEN );
        syslog( LOG_INFO, "Sending %s", strip );
    }
    else
    {
        resp.cmd = LEASE_ERR;
        syslog( LOG_WARNING, "No available address to lease" );
    }

    req = ( write_req_t * ) malloc( sizeof( write_req_t ));
    req->buf = uv_buf_init(( char * ) malloc( sizeof( msg_proto_t )), sizeof( msg_proto_t ));
    memcpy( req->buf.base, &resp, sizeof( msg_proto_t ));
    uv_write(( uv_write_t * ) req, dest, &req->buf, 1, on_write );
}
