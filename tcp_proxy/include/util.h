/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef TCP_PROXY_UTIL_H_
#define TCP_PROXY_UTIL_H_

const char *get_prog_name();

/* return 1 on success, 0 on failure */
int add_ip(const char *ip, const char *iface);
int del_ip(const char *ip, const char *iface);

#if defined(__GNUC__)
# define ATTRIBUTE_FORMAT_PRINTF(a, b) __attribute__((format(printf, a, b)))
#else
# define ATTRIBUTE_FORMAT_PRINTF(a, b)
#endif
void pr_info(const char *fmt, ...) ATTRIBUTE_FORMAT_PRINTF(1, 2);
void pr_warn(const char *fmt, ...) ATTRIBUTE_FORMAT_PRINTF(1, 2);
void pr_err(const char *fmt, ...) ATTRIBUTE_FORMAT_PRINTF(1, 2);
void pr_debug(const char *fmt, ...) ATTRIBUTE_FORMAT_PRINTF(1, 2);
void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t size);
void *xcalloc(size_t nmemb, size_t size);

#endif /* TCP_PROXY_UTIL_H_ */
