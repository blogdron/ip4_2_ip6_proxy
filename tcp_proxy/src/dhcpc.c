/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include <config.h>
#include <util.h>

enum {
    LEASE     = 1,
    RELEASE   = 2,
    LEASE_OK  = 4,
    LEASE_ERR = 8,
};

typedef struct {
    int cmd;
    struct in6_addr ip;
} msg_proto_t;

typedef union {
    struct sockaddr_in tcp_srv;
    struct sockaddr_un unix_srv;
} socket_t;

static int create_socket(const tcp_proxy_config_t *);
static int make_connect(const tcp_proxy_config_t *, socket_t *, int);

char *get_ipv6_addr()
{
    char *ip6addr;
    int fd, ret;
    socket_t server;
    msg_proto_t msg, msg_reply;

    const tcp_proxy_config_t *config = get_config();
    if (!config)
    {
        return NULL;
    }

    ip6addr = (char *) xmalloc(INET6_ADDRSTRLEN);

    fd = create_socket(config);
    if (fd == -1)
    {
        pr_err("create_socket failed");
        return NULL;
    }

    ret = make_connect(config, &server, fd);
    if (ret < 0)
    {
        pr_err("make_connect failed");
        return NULL;
    }
    msg.cmd = LEASE;

    if (send(fd, (void *) &msg, sizeof(msg), 0) < 0)
    {
        pr_err("send failed: %s", strerror(errno));
        return NULL;
    }

    if (recv(fd, &msg_reply, sizeof(msg_reply), 0) != sizeof(msg_reply))
    {
        pr_err("recv failed: %s", strerror(errno));
        return NULL;
    }

    if (msg_reply.cmd & LEASE_OK)
    {
         if (!inet_ntop(AF_INET6, &msg_reply.ip, ip6addr, INET6_ADDRSTRLEN))
         {
             pr_err("inet_ntop failed: %s", strerror(errno));
         }
    }
    if (msg_reply.cmd & LEASE_ERR)
    {
        pr_warn("no free ip addresses now");
        ip6addr = NULL;
    }

    close(fd);

    return ip6addr;
}

static int create_socket(const tcp_proxy_config_t *config)
{
    int fd;

    if (config->dhcpd_sock_family == AF_INET)
    {
        fd = socket(AF_INET, SOCK_STREAM, 0);
    }
    else if (config->dhcpd_sock_family == AF_UNIX)
    {
        fd = socket(AF_UNIX, SOCK_STREAM, 0);
    }

    if (fd == -1)
    {
        pr_err("socket failed: %s", strerror(errno));
    }

    return fd;
}

static int make_connect(const tcp_proxy_config_t *config, socket_t *server, int fd)
{
    int ret;

    if (config->dhcpd_sock_family == AF_INET)
    {
        server->tcp_srv.sin_addr.s_addr = inet_addr(config->dhcpd_host);
        server->tcp_srv.sin_family = AF_INET;
        server->tcp_srv.sin_port = htons(config->dhcpd_port);
        ret = connect(fd, (struct sockaddr *) &server->tcp_srv, sizeof(server->tcp_srv));
    }
    else if (config->dhcpd_sock_family == AF_UNIX)
    {
        strncpy(server->unix_srv.sun_path, config->dhcpd_host, sizeof(server->unix_srv.sun_path));
        server->unix_srv.sun_family = AF_UNIX;
        ret = connect(fd, (struct sockaddr *) &server->unix_srv, sizeof(server->unix_srv));
    }

    if (ret < 0)
    {
        pr_err("connect failed: %s", strerror(errno));
    }

    return ret;
}
