/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdlib.h>
#include <stdio.h>
#include <search.h>
#include "iht.h"
#include "util.h"

#ifdef IHT_TABLE_SIZE_
#undef IHT_TABLE_SIZE_
#endif /* IHT_TABLE_SIZE_ */

#define IHT_TABLE_SIZE_ 31

typedef void *primary_table_t[IHT_TABLE_SIZE_];

static int compare_keys(const void *lhs, const void *rhs);
/* static void print_table(void *iht); */

void print_table(void *iht) {
    size_t i;
    for(i = 0; i < IHT_TABLE_SIZE_; ++i) {
        pr_debug("idx: %zu, pointer: 0x%X", i, ((void **)iht)[i]);
    }
}

static int compare_keys(const void *lhs, const void *rhs) {
    iht_key_t *l, *r;

    l = (iht_key_t *) (lhs);
    r = (iht_key_t *) (rhs);

    if (*l < *r) {
        return -1;
    }
    else if (*l > *r) {
        return 1;
    }
    else {
        return 0;
    }
}

void *iht_create(void) {
    return xcalloc(IHT_TABLE_SIZE_, sizeof(void *));
}

void *iht_insert(void *iht, void *node) {
    iht_key_t *key = (iht_key_t*) (node); 
    unsigned idx = -1;
    void *ret;

    if (NULL == iht) {
        pr_err("NULL table pointer");
        return NULL;
    }
    if (NULL == node) {
        pr_err("NULL node to be inserted");
        return NULL;
    }
    pr_debug("Trying to insert node with key: %lu", *key);

    idx = *key % IHT_TABLE_SIZE_;
    ret = tsearch(node, &(((void **)iht)[idx]), compare_keys);
    return ret;
}

void *iht_find(void *iht, iht_key_t key) {
    unsigned idx = key % IHT_TABLE_SIZE_;
    return tfind((const void *)&key, (void *const *)&(((void **)iht)[idx]), compare_keys);
}
