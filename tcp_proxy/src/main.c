/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uv.h>
#include <libgen.h>
#include <syslog.h>
#include <unistd.h>

#include "util.h"
#include "iht.h"
#include "config.h"
#include "dhcpc.h"

static const char *progname = __FILE__; /* reset in main() */
static const char *pid_path = "/var/run/tcp_proxy.pid";

static const tcp_proxy_config_t *cfg = NULL;
static uv_loop_t *loop;
static uint8_t global_stop = 0;

struct child_worker {
    uv_process_t req;
    uv_process_options_t options;
    uv_pipe_t pipe;
} *workers;

typedef struct port_info_ {
    iht_key_t port;
    time_t last_changed_addr;
    char *ip6_addr;
    char *iface;
} port_info;

static int round_robin_counter;
static int child_worker_count;

static uv_buf_t pipe_buf;
static char worker_path[500];
static const char manager_bin_name[] = "tcp_proxy";
static const char worker_bin_name[] = "tcp_proxy_worker";
static void *iht = NULL;

static void conn_close(uv_handle_t *handle);
static void write_req_cb(uv_handle_t *handle, int status);
static void close_process_handle(uv_process_t *req, int64_t exit_status, int term_signal);
static unsigned short connected_port(uv_tcp_t *server);
static char *get_random_iface();
static int need_request_ipv6(port_info *pi);
static void on_new_connection(uv_stream_t *server, int status);
static void handle_stop(uv_signal_t *handle, int signum);
static void init_signals();
static void setup_workers();
static void run_worker(struct child_worker *worker);
static void stop_workers();
static void prepare_config(int argc, char **argv);
static void cleanup();
static void run_as_daemon();

int main(int argc, char **argv) {

    progname = basename(argv[0]);

    if(0 != atexit(cleanup)) {
        pr_err("Can't register cleanup function");
        exit(EXIT_FAILURE);
    }

    prepare_config(argc, argv);
    run_as_daemon();
    dump_config();
    loop = uv_default_loop();

    init_signals();

    uv_tcp_t **server = (uv_tcp_t **) xcalloc(cfg->ports_count, sizeof(*server));
    int i;
    for (i = 0; i < cfg->ports_count; ++i)
    {
        server[i] = (uv_tcp_t *) xcalloc(1, sizeof(uv_tcp_t));
    }

    pr_debug("Allocating port table");
    iht = iht_create();
    if (NULL == iht) {
        pr_err("Failed to create port table");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < cfg->ports_count; ++i)
    {
        pr_debug("bind to port %ld", cfg->port_range[i]);
        uv_tcp_init(loop, server[i]);

        struct sockaddr_in bind_addr;
        uv_ip4_addr(cfg->host, cfg->port_range[i], &bind_addr);

        int r;
        if ((r = uv_tcp_bind(server[i], (const struct sockaddr *)&bind_addr, 0))) {
            pr_err("Bind error: %s", uv_err_name(r));
            if (r == -EMFILE)
            {
                pr_err("too many open files, use \"ulimit -n count\" to fix it");
            }
            exit(EXIT_FAILURE);
        }

        pr_debug("Inserting port info for %ld", cfg->port_range[i]);
        port_info *pi = xcalloc(1, sizeof(*pi));
        pi->port = cfg->port_range[i];
        (void)iht_insert(iht, (void *)pi);

        if ((r = uv_listen((uv_stream_t*) server[i], 128, on_new_connection))) {
            pr_err("Listen error: %s", uv_err_name(r));
            exit(EXIT_FAILURE);
        }
    }

    setup_workers();

    uv_run(loop, UV_RUN_DEFAULT);
    exit(EXIT_SUCCESS);
}

static void run_worker(struct child_worker *worker) {
    size_t path_size = 500;
    uv_exepath(worker_path, &path_size);
    strcpy(worker_path + (strlen(worker_path) - sizeof(manager_bin_name) + 1), worker_bin_name);

    pr_info("Worker path: %s", worker_path);

    char* args[2];
    args[0] = worker_path;
    args[1] = NULL;
    uv_pipe_init(loop, &worker->pipe, 1);

    uv_stdio_container_t child_stdio[3];
    child_stdio[0].flags = UV_CREATE_PIPE | UV_READABLE_PIPE;
    child_stdio[0].data.stream = (uv_stream_t*) &worker->pipe;
    child_stdio[1].flags = UV_IGNORE;
    child_stdio[2].flags = UV_INHERIT_FD;
    child_stdio[2].data.fd = 2;

    worker->options.stdio = child_stdio;
    worker->options.stdio_count = 3;

    worker->options.exit_cb = close_process_handle;
    worker->options.file = args[0];
    worker->options.args = args;

    if(uv_spawn(loop, &worker->req, &worker->options)) {
        pr_err("%s", uv_strerror(loop));
    } else {
        pr_info("Started worker %d", worker->req.pid);
    }
}

static void conn_close(uv_handle_t *handle) {
    pr_debug("Closing connection handle %p", handle);
    free(handle);
}
static void write_req_cb(uv_handle_t *handle, int status) {
    pr_debug("Write request for worker finished with status: %d", status);
    free(handle);
}
static void close_process_handle(uv_process_t *req, int64_t exit_status, int term_signal) {
    pr_info("Process with pid=%d exited with status %" PRId64 ", signal %d", req->pid, exit_status, term_signal);
    if(!global_stop) {
        for(size_t i = 0; i < child_worker_count; ++i) {
            if (workers[i].req.pid == req->pid && exit_status == 0) {
                run_worker(&workers[i]);
                return;
            }
        }
    }
  
    uv_close((uv_handle_t*) req, NULL);
}

/* Return 0 on failure, port number on success */
static unsigned short connected_port(uv_tcp_t *server) {
    int res;
    struct sockaddr addr;
    int namelen = sizeof(struct sockaddr); /* dummy, not used actually */
    unsigned short port = 0;

    if (!server) {
        goto cleanup;
    }
    if (0 != (res = uv_tcp_getsockname(server, &addr, &namelen))) {
        pr_err("Failed to get incoming connection info: %s", uv_err_name(res));
        goto cleanup;
    }

    port = htons(((struct sockaddr_in *)&addr)->sin_port);
    pr_debug("Connection port: %hu", port);
cleanup:
    return port;
}

static char *get_random_iface() {
    if(cfg->num_ifs == 1) {
        return cfg->out_ifs[0];
    }
    srand(time(NULL));
    size_t i = (rand() % cfg->num_ifs);
    return cfg->out_ifs[i];
}

static int need_request_ipv6(port_info *pi) {
    if (NULL == pi) {
        pr_err("NULL port info");
        return 0;
    }
    pr_debug("Checking last changed addr time for port %hu", pi->port);
    port_info *pi_ = (port_info **)pi;
    time_t now = time(NULL);
    time_t last_changed = pi_->last_changed_addr;
    if ((now - last_changed) >= cfg->addr_change_timeout) {
        pr_debug("Timeout for addr change for port %hu reached", pi_->port);
        return 1;
    }
    pr_debug("Timeout for addr change for port %hu not reached", pi_->port);
    return 0;
}

static void on_new_connection(uv_stream_t *server, int status) {
    char *ip6_addr = NULL;
    uv_tcp_t *client = NULL;
    int need_close_client = 0;
    uv_os_fd_t fd = -1;

    if (status == -1) {
        // error!
        goto cleanup;
    }

    client = (uv_tcp_t*) xmalloc(sizeof(*client));
    uv_tcp_init(loop, client);
    if (uv_accept(server, (uv_stream_t*) client) == 0) {
        uv_fileno((uv_handle_t *) client, &fd);
        pr_debug("Accepted fd: %d", fd);
        if(fd <= 0) {
            pr_err("BUG: invalid accepted fd: %d!", fd);
            need_close_client = 0;
            goto cleanup;
        }
        time_t now = time(NULL);
        iht_key_t port = (iht_key_t)connected_port(server);
        void *pi_found = iht_find(iht, port);
        if (NULL == pi_found) {
            pr_err("Can't find port info for port %hu", port);
            need_close_client = 1;
            goto cleanup;
        }
        port_info *pi = *(port_info **)pi_found;
        if(need_request_ipv6(pi)) {
            ip6_addr = get_ipv6_addr();
            if (NULL == ip6_addr) {
                pr_warn("Failed to get ipv6 addr from server");
            }
            else {
                char *old_addr = pi->ip6_addr;
                char *old_if = pi->iface;
                char *new_if = get_random_iface();
                pr_debug("recieved IPv6 address: '%s', old: '%s'", ip6_addr, old_addr);
                pr_debug("Updating last_changed_addr timestamp for port %hu to %lu", port, (unsigned long)now);
                pi->last_changed_addr = now;
                /*
                 * TODO: Currently we hardcode the subnet postfix for addrs
                 * We need to get rid of it in future
                 */
                char addrbuf[64];
                if(old_addr && old_if) {
                    snprintf(addrbuf, sizeof(addrbuf), "%s/64", old_addr);
                    if(!del_ip(addrbuf, old_if)) {
                        pr_warn("Failed to remove addr '%s' from '%s'", old_addr, old_if);
                    }
                    free(pi->ip6_addr);
                }
                snprintf(addrbuf, sizeof(addrbuf), "%s/64", ip6_addr);
                if(!add_ip(addrbuf, new_if)) {
                    pr_err("Failed to add addr '%s' to '%s'", ip6_addr, new_if);
                    need_close_client = 1;
                    goto cleanup;
                }
                pr_debug("Saving addr '%s' and iface '%s' for port %hu", ip6_addr, new_if, port);
                pi->ip6_addr = strdup(ip6_addr);
                pi->iface = new_if;
            }
        }
        if (NULL != ip6_addr) {
            pipe_buf = uv_buf_init(ip6_addr, strlen(ip6_addr));
        }
        else if (NULL != pi->ip6_addr) {
            pipe_buf = uv_buf_init(pi->ip6_addr, strlen(pi->ip6_addr));
        }
        else {
            pipe_buf = uv_buf_init("n", 1);
        }
        struct child_worker *worker = &workers[round_robin_counter];
        uv_write_t *write_req = (uv_write_t*) xmalloc(sizeof(*write_req));
        uv_write2(write_req, (uv_stream_t*) &worker->pipe, &pipe_buf, 1, (uv_stream_t*) client, write_req_cb);
        round_robin_counter = (round_robin_counter + 1) % child_worker_count;
    }
cleanup:
//    if (need_close_client) {
    pr_debug("Closing socket: %d", fd);
    uv_close((uv_handle_t*) client, conn_close);
//    }
    if (ip6_addr) {
        free(ip6_addr);
    }
}

static void setup_workers() {
    round_robin_counter = 0;

    child_worker_count = cfg->num_workers;

    if(0 == child_worker_count) {
        // launch same number of workers as number of CPUs
        uv_cpu_info_t *info;
        int cpu_count;
        uv_cpu_info(&info, &cpu_count);
        uv_free_cpu_info(info, cpu_count);

        child_worker_count = cpu_count;
    }

    pr_info("Set number of workers to %d", child_worker_count);

    workers = xcalloc(child_worker_count, sizeof(*workers));
    int i = child_worker_count;
    while (i--) {
        run_worker(&workers[i]);
    }
}

static void stop_workers() {
    pr_debug("Stopping worker processes");
    for(int i = 0; i < child_worker_count; ++i) {
        uv_process_kill(&workers[i].req, SIGTERM);
        /* nothing to do here: worker exit_cb will take care of handle */
    }
}

static void prepare_config(int argc, char **argv) {
    if(-1 == alloc_config()) {
        pr_err("Can't initialize configuration");
        exit(EXIT_FAILURE);
    }
    cfg = get_config();

    if(argc == 1) {
        set_config_defaults();
        return;
    }

    if(argc > 1 && -1 == parse_command_line(argc, argv)) {
        pr_err("Failed to parse command line");
        exit(EXIT_FAILURE);
    }

    if('\0' != *get_config_filename() && -1 == parse_config_file(get_config_filename())) {
        pr_err("Can't parse config file");
        exit(EXIT_FAILURE);
    }
}

static void handle_stop(uv_signal_t *handle, int signum) {
    pr_info("Received signal: %d", signum);
    global_stop = 1;
    stop_workers();
    uv_stop(loop);
    if (unlink(pid_path) == -1)
    {
        syslog(LOG_WARNING, "Cannot delete pid file %s : %s",
               pid_path, strerror(errno));
    }
}

static void init_signals() {
    static uv_signal_t sigint, sigterm, sigquit;

    uv_signal_init(loop, &sigint);
    uv_signal_start(&sigint, handle_stop, SIGINT);

    uv_signal_init(loop, &sigterm);
    uv_signal_start(&sigterm, handle_stop, SIGTERM);

    uv_signal_init(loop, &sigquit);
    uv_signal_start(&sigquit, handle_stop, SIGQUIT);

    struct sigaction pipe_act = {0};
    pipe_act.sa_handler = SIG_IGN;
    sigfillset(&pipe_act.sa_mask);

    int res = sigaction(SIGPIPE, &pipe_act, NULL);
    if(0 != res) {
        pr_err("sigaction() for SIGPIPE failed: %s", strerror(errno));
    }
}

static void cleanup() {
    tcp_proxy_config_t *cfg = get_config();
    if(cfg != NULL)
    {
        if(cfg->port_range != NULL)
        {
            free(cfg->port_range);
        }
    }
    pr_debug("Cleaning up");
    free_config();
}

const char *get_prog_name() {
    return progname;
}

static void run_as_daemon()
{
    if (cfg->daemon_mode)
    {
        printf("Forking to background\n");
        if (daemon(0, 0) != 0)
        {
            fprintf(stderr, "detach failed: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        else
        {
            FILE *fp;
            fp = fopen(pid_path, "w");
            if (fp == NULL)
            {
                syslog(LOG_WARNING, "Cannot create pid file %s : %s",
                       pid_path, strerror(errno));
            }
            else
            {
                fprintf(fp, "%d", getpid());
                fclose(fp);
            }
        }
    }
}
