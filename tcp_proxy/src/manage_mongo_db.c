/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>

#include <bson.h>
#include <json.h>

#include "manage_mongo_db.h"

int check_user(mongoc_client_pool_t* pool, const char* mongo_db, const char* user, const char* pass)
{
    mongoc_client_t *client;
    mongoc_collection_t *collection_w;
    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;
    int res = 0;

    client = mongoc_client_pool_pop( pool );
    if( !client )
    {
        // Connection pooling failed
        return 1;
    }

    collection_w = mongoc_client_get_collection( client, mongo_db, COLLECTION_DB_USERS );
    query = bson_new();
    BSON_APPEND_UTF8 (query, user, pass);

    cursor = mongoc_collection_find( collection_w, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL );
    if ( !mongoc_cursor_next( cursor, &doc ))
    {
        mongoc_cursor_destroy( cursor );
        res = 1;
        goto cleanup;
    }

    mongoc_cursor_destroy( cursor );

cleanup:
    bson_destroy( query );
    mongoc_collection_destroy( collection_w );
    mongoc_client_pool_push( pool, client );

    return res;
}
