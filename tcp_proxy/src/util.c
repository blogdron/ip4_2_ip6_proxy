/* vim: set tabstop=4 shiftwidth=4 expandtab : */
/* Copyright StrongLoop, Inc. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "defs.h"
#include "util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

static void pr_do(FILE *stream,
                    const char *label,
                    const char *fmt,
                    va_list ap);

static int change_ip(const char *ip, const char *iface, int del);

int add_ip(const char *ip, const char *iface) {
    return change_ip(ip, iface, 0);
}

int del_ip(const char *ip, const char *iface) {
    return change_ip(ip, iface, 1);
}

/* del == 1 - remove ip, del == 0 - add */
/* return 1 on success, 0 on failure */
#define CMD_SIZE_ 128
static int change_ip(const char *ip, const char *iface, int del) {
    char cmd[CMD_SIZE_];
    int exit_code = -1;
    char buf[256] = {0};
    FILE *fp = NULL;
    char c = 0;

    if (!ip) {
        return 0;
    }
    if (!iface) {
        return 0;
    }

    c = del ? 'd' : 'a';

    snprintf(cmd, sizeof(cmd) - 1, "ip a %c %s dev %s", c, ip, iface);
    fp = popen(cmd, "r");
    if (!fp) {
        pr_err("Failed to spawn '%s' command: %s", cmd, strerror(errno));
        return 0;
    }
    (void)fgets(buf, sizeof(buf), fp);
    exit_code = WEXITSTATUS(pclose(fp));
    if(exit_code) {
        pr_err("Failed to assign ip: %s", buf);
        return 0;
    }

    return 1;
}
#undef CMD_SIZE_

void *xmalloc(size_t size) {
    void *ptr;

    ptr = malloc(size);
    if (ptr == NULL) {
      pr_err("out of memory, need %lu bytes", (unsigned long) size);
      exit(1);
    }

    return ptr;
}

void *xcalloc(size_t nmemb, size_t size) {
    void *ptr;

    ptr = calloc(nmemb, size);
    if (ptr == NULL) {
      pr_err("out of memory, need %lu bytes", (unsigned long) size);
      exit(1);
    }

    return ptr;
}

void *xrealloc(void *ptr, size_t size) {
    void *ptr_;

    ptr_ = realloc(ptr, size);
    if (ptr_ == NULL) {
      pr_err("out of memory, need %lu bytes", (unsigned long) size);
      exit(1);
    }

    return ptr_;
}

void pr_info(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    pr_do(stdout, "info", fmt, ap);
    va_end(ap);
}

void pr_warn(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    pr_do(stderr, "warn", fmt, ap);
    va_end(ap);
}

void pr_err(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    pr_do(stderr, "error", fmt, ap);
    va_end(ap);
}

void pr_debug(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    pr_do(stderr, "debug", fmt, ap);
    va_end(ap);
}

static void pr_do(FILE *stream,
                    const char *label,
                    const char *fmt,
                    va_list ap) {
    char fmtbuf[1024];
    vsnprintf(fmtbuf, sizeof(fmtbuf), fmt, ap);
    fprintf(stream, "%s:%lu:%s: %s\n", get_prog_name(), getpid(), label, fmtbuf);
}
