use FindBin qw/ $Bin /;
use lib "$Bin/../testlib";
use Test::More qw/ no_plan /;
use Helpers;
use strict;
use warnings;

END {
    Helpers::stop_server('SIGKILL');
    Helpers::stop_proxy('SIGKILL');
}

my $self_test_errors = 0;

{
my $err = Helpers::start_server();
ok(!$err, 'Server started');
++$self_test_errors if $err;

BAIL_OUT("Can't even start server. Nothing to test") if $err;

$err = Helpers::send_valid_request();
ok(!$err, 'Server responded to valid request');
++$self_test_errors if $err;

$err = Helpers::send_invalid_request();
ok(!$err, 'Server responded with error to invalid request');
++$self_test_errors if $err;

$err = Helpers::stop_server();
ok(!$err, 'Server stopped');
++$self_test_errors if $err;
}

ok(!$self_test_errors, "Upstream server self-test passed");
BAIL_OUT("Upstream test server can't pass self-test") if $self_test_errors;

{
my $err = Helpers::start_proxy({ proxy_port => 7000 });
ok(!$err, 'Proxy started');
BAIL_OUT("Can't start proxy") if $err;

$err = Helpers::start_server();
BAIL_OUT("Can't start server") if $err;

$err = Helpers::send_valid_request('proxy');
ok(!$err, "Proxy works with valid request");

$err = Helpers::send_invalid_request('proxy');
ok(!$err, "Proxy work with invalid request");

$err = Helpers::stop_server();
BAIL_OUT("Can't stop server") if $err;

$err = Helpers::send_valid_request('proxy');
ok($err && Helpers::proxy_alive(), "Proxy gives error and still alive without upstream");

$err = Helpers::stop_proxy();
ok(!$err, 'Proxy stopped');
}
