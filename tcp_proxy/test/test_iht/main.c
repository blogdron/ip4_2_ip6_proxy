/* vim: set tabstop=4 shiftwidth=4 expandtab : */

#include <stdio.h>
#include <time.h>
#include "iht.h"

typedef struct Node_ {
	iht_key_t key;
	time_t timestamp;
} Node;

int main(void) {
	void *iht = NULL;
	void *inserted = NULL;
	Node *nodep = NULL;
	Node node;
	node.key = 12345;
	node.timestamp = time(NULL);
	printf("timestamp: %lu\n", (unsigned long)node.timestamp);

	iht = iht_create();

	if(!(inserted = iht_insert(iht, &node))) {
		fprintf(stderr, "Failed to insert node into table\n");
		return 1;
	}

	printf("Inserted timestamp: %lu\n", (*(Node **)inserted)->timestamp);

	nodep = *(Node **)iht_find(iht, 12345);
	if (NULL == nodep) {
		fprintf(stderr, "Failed to find inserted node\n");
		return 1;
	}

	printf("key: %lu, timestamp: %lu\n", (unsigned long)nodep->key, (unsigned long)nodep->timestamp);

	return 0;
}

